<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    /**
     * @return mixed
     */
    public function productLine()
    {
        return $this->belongsTo(ProductLine::class, 'productLine', 'productLine');
    }
}
