<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'orderdetails';

    /**
     * @return mixed
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'productCode', 'productCode');
    }

    public function lineTotal()
    {
        return number_format($this->priceEach * $this->quantityOrdered, 2);
    }

    public function getBillAmountAttribute($value)
    {
        return 11;
    }
}