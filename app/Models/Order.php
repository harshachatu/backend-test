<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{

    protected $table = 'orders';


    /**
     * @return mixed
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customerNumber' , 'customerNumber');
    }

    /**
     * Get the associated order details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class, 'orderNumber', 'orderNumber');
    }

     /**
     */
    public function billAmount()
    {
        return $this->hasMany(OrderDetail::class, 'orderNumber', 'orderNumber')->sum(\DB::raw('priceEach * quantityOrdered'));
    }
}

