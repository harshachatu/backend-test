<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'product' => $this->product->productName,
            'product_line' => $this->product->productLine,
            'unit_price' => $this->priceEach,
            'qty' => $this->quantityOrdered,
            'line_total' => $this->lineTotal(),
        ];
    }

}