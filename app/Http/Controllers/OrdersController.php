<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;

class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        $order = Order::query()
            ->with([
                'orderDetails',
                'customer'
            ])
            ->where('orderNumber', '=', $id)
            ->firstOrFail();
                 
        return new OrderResource($order);         
    }
}
